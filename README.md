# Monitoring cluster Kubernetes avec le dashboard Kubernetes, prometheus and Grafana

 
> Cette procèdure fonctionne sur tout systeme ayant `Docker-Destop` installé.

> Vous trouverez tous les fichiers YAML dans le dossier [k8s](k8s) avec leur origine et les modifications appliquées nécessaires.


## Kubernetes

* Nous utiliserons 'Docker Desktop' qui est livré avec un cluster Kubernetes prêt à l’emploi.


* Sur lequel, nous avons activé 'Kubernetes' dans les settings.

![Docker for Desktop with Kubernetes](img/docker-desktop.png)

* Pour verifierque tout fonctionne bien, entrez la commande suivante :

```
% kubectl get all -A
```

## Kubernetes Dashboard

`Dashboard` est une interface utilisateur web de l’API k8s et fournit un moyen facile de visualiser et déboguer les objets kube. Vous pouvez en savoir plus à l’adresse suivante :
  https://github.com/kubernetes/dashboard
  

### Installation

Pour déployer Dashboard - sans authentification - exécutez la commande suivante :

> Note :
> Le fichier de configuration a été modifié pour permettre de contourner l'authentification par jeton (ligne 198).

Pour déployer le `Dashboard` - sans authentification - exécutez la commande suivante :

```
% kubectl apply -f ./k8s/dashboard-v2.2.0-recommended.yaml
```

Accédez ensuite à votre tableau de bord depuis votre poste de travail local en créant un canal sécurisé vers votre cluster Kubernetes. Pour faire donc, exécuter la commande suivante:

```
% kubectl proxy
```

L’adresse est à présent:

http://localhost:8001/api/v1/namespaces/kubernetes-dashboard/services/https:kubernetes-dashboard:/proxy/

> Note :
>
> /!\ A mettre dans vos favoris!

Vous verrez d’abord cette page de connexion :

![Dashboard first Access page](img/dashboard-first-access.jpg)

Il suffit d’appuyer sur le bouton 'skip' pour contourner l’authentification.

![Dashboard index page](img/dashboard-index.jpg)

## Metrics-Server

'Metrics-Server' sert à extraire les mesures des composants k8s et les expose à l’API k8s. Le repo officiel est https://github.com/kubernetes-sigs/metrics-server

`Metrics Server` sert à mesurer les informations de base : CPU et RAM. Il s’agit d’un composant de niveau cluster qui récolte périodiquement les mesures de tous les nœuds Kubernetes suivis par Kubelet. Horizontal Pod Autoscaler utilise cette API pour collecter des mesures.

> Note :
>
> Le fichier de configuration a été modifié pour permettre à `Metrics Server` de collecter de données sur https en acceptant les connexions TLs non sécuriséesen ajoutant `- --kubelet-insecure-tls` (ligne 133).

Appliquez la configuration en entrant :

```
% kubectl apply -f k8s/metrics-server-components-v0.4.2.yaml
```

Lors du rechargement du Dashboard, vous devriez maintenant voir les utilisations du processeur et de la mémoire (après un certain temps) 
.
La section `Pods` est plutôt sympa 🌈! 

![Dashboard with metrics page](img/dashboard-with-metrics.jpg)

## Kube State Metrics

Contrairement à `Metrics Server`, `Kube State Metrics` se concentre sur la génération de metrics à partir de l’état des objets Kubernetes : Deployments, Replicas, Pods, etc.).
Pour cela, il gère, en mémoire, une photo des états de Kubernetes et génère de nouvelles mesures basées sur celle-ci.

En installant `Kube State Metrics`, cela permezt l'accés à, ces mesures à partir de sytèmes de surveillance tels que `Prometheus`.

Pour installer Kube State Metrics, lancez la commande suivante :

```
% kubectl apply -f k8s/kube-state-metrics-v2.0.0-rc.1.yaml
```

## Prometheus

Prometheus est un système de collecte, d’interrogation, de surveillance et d’alerte. Il est utile quand il s’agit de :

- Collecter des données identifiées par un nom de métrique
- Stocker les séries chronologiques en mémoire et sur disque pour plus d’efficacité
- Lancer des notifications et des alertes en fonction des requêtes de données

La documentation complète est accessible à partir de https://prometheus.io.

Les développeurs de Prometheus fournissent des binaires et des images Docker des composants de Prometheus. Avec un peu d’huile de coude, il est possible de créer un fichier de configuration k8s avec tout ce dont nous avons besoin : accès aux ressources, rôle dédié, configuration, déploiement et exposition de service.

Pour installer la configuration Prometheus, exécutez la commande :

```
% kubectl apply -f k8s/prometheus.yaml
```
Vous pouvez accéder à l’interface de Prometheus à l’adresse [http://localhost:30000/](http://localhost:30000/)... 

## Grafana

`Grafana` [https://grafana.com/grafana/](https://grafana.com/grafana/) vous permet d’ « interroger, visualiser et alerter en fontion des mesures au moyen d’une interface utilisateur puissante » dixit leur site.

Dans la pratique, vous développerez votre requête dans `Prometheus` puis la lancerez dans `Grafana` pour monitorer vos services.

Comme vous pouvez le voir l'interface graphique de `Grafana` permet de nombreuses possibilités :

![L'interface Grafana](img/grafana-ui.png)

Pour installer `Grafana` et configurer la source de données `Prometheus`, exécutez les commandes suivantes :

```
% kubectl apply -f k8s/grafana-datasource.yaml
% kubectl apply -f k8s/grafana.yaml
```

> Note:
> 
> Configurer un tableau de bord dans Grafana peut être long et chronophage. La collecte de mesures dépend de votre hôte, de l’utilisation de VM ou solution de virtualisation et du système d’exploitation sur votre pod. Vous devrez mettre les mains dans le cambouis…

Afin de vous simplifier la vie, vous trouverez dans le dossier `k8s` un fichier json pour un `Dashboard` à importer dans l'interface graphique der Grafana. 
